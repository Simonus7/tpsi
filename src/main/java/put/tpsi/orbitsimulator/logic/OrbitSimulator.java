/*
 * Created on 10-03-2012
 * Autor Radosław Ziembiński, ZISWD, Politechnika Poznańska, 2012
 * Materiał dydaktyczny, przeznaczony wyłacznie do celów ćwiczeniowych z przedmiotu Technologie programistyczne - systemy internetowe.
 */

package put.tpsi.orbitsimulator.logic;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import put.tpsi.orbitsimulator.entities.BodyPosition;
import put.tpsi.orbitsimulator.entities.SimulationAnimation;
import put.tpsi.orbitsimulator.entities.SimulationRequest;

/**
 * Symulator sondy
 * 
 * @author Radosław Ziembiński, 2012
 */
public final class OrbitSimulator implements Serializable {

	/**
	 * Wspólna definicja obiektu w symulacji
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected static class Body implements Serializable {
		/**
		 * Położenie X obiektu [m]
		 */
		public double x = 0.0;

		/**
		 * Położenie y obiektu [m]
		 */
		public double y = 0.0;

		/**
		 * Położenie z obiektu [m]
		 */
		public double z = 0.0;

		/**
		 * Masa obiektu [kg]
		 */
		public double mass = 1.0;

		/**
		 * Promień obiektu [m]
		 */
		public double radius = 1.0;

		/**
		 * Wypadkowa siła działająca na obiekt względem osi X [N]
		 */
		public double fx = 0.0;

		/**
		 * Wypadkowa siła działająca na obiekt względem osi Y [N]
		 */
		public double fy = 0.0;

		/**
		 * Wypadkowa siła działająca na obiekt względem osi Z [N]
		 */
		public double fz = 0.0;

		/**
		 * Comment for <code>serialVersionUID</code>
		 */
		private static final long serialVersionUID = 2528256132693937735L;
	}

	/**
	 * Pojedyncza instrukcja dla silnika sondy aplikowana w trakcie lotu
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Instruction implements Serializable {

		private static final long serialVersionUID = -7140000382391789687L;

		/**
		 * Moment działania w czasie lotu w jednostkach całkowitych
		 */
		public int tick;

		/**
		 * Ciąg względem osi X [-1; 1]
		 */
		public double tx = 0.0;

		/**
		 * Ciąg względem osi Y [-1; 1]
		 */
		public double ty = 0.0;

		/**
		 * Ciąg względem osi Z [-1; 1]
		 */
		public double tz = 0.0;

		/**
		 * Tworzy kopie obiektu
		 * 
		 * @return kopia instrukcji
		 * @author Radosław Ziembiński
		 */
		public Instruction cloneInstruction() {
			final Instruction copy = new Instruction();
			copy.tick = this.tick;
			copy.tx = this.tx;
			copy.ty = this.ty;
			copy.tz = this.tz;
			return copy;
		}

		/**
		 * Losowo modyfikuje instrukcję lotu
		 * 
		 * @author Radosław Ziembiński
		 */
		public void modifyRandom() {
			double value = OrbitSimulator.random.nextDouble();
			if (value < 0.75) {
				value = OrbitSimulator.random.nextDouble();
				if (value < 0.25) {
					tx = 2.0 * OrbitSimulator.random.nextDouble() - 1.0;
				} else if (value < 0.50) {
					ty = 2.0 * OrbitSimulator.random.nextDouble() - 1.0;
				} else if (value < 0.75) {
					tz = 2.0 * OrbitSimulator.random.nextDouble() - 1.0;
				} else {
					tick = OrbitSimulator.random
							.nextInt(OrbitSimulator.TICKS_MAX);
				}
			} else {
				value = OrbitSimulator.random.nextDouble();
				if (value < 0.25) {
					tx += OrbitSimulator.random.nextDouble() * 0.1 - 0.05;
					tx = (tx < -1) ? (-1) : ((tx > 1) ? (1) : (tx));
				} else if (value < 0.50) {
					ty += OrbitSimulator.random.nextDouble() * 0.1 - 0.05;
					ty = (ty < -1) ? (-1) : ((ty > 1) ? (1) : (ty));
				} else if (value < 0.75) {
					tz += OrbitSimulator.random.nextDouble() * 0.1 - 0.05;
					tz = (tz < -1) ? (-1) : ((tz > 1) ? (1) : (tz));
				} else {
					tick = OrbitSimulator.random.nextInt(1000) - 500;
					tick = (tick < 0) ? (0)
							: ((tick > OrbitSimulator.TICKS_MAX) ? (OrbitSimulator.TICKS_MAX)
									: (tick));
				}
			}
		}

		/**
		 * Tworzy losową instrukcję lotu
		 * 
		 * @author Radosław Ziembiński
		 */
		public void random() {
			tx = OrbitSimulator.random.nextDouble() * 2.0 - 1.0;
			ty = OrbitSimulator.random.nextDouble() * 2.0 - 1.0;
			tz = OrbitSimulator.random.nextDouble() * 2.0 - 1.0;
			tick = OrbitSimulator.random.nextInt(OrbitSimulator.TICKS_MAX);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			final StringBuffer buffer = new StringBuffer();
			buffer.append("tick=").append(tick).append(' ');
			buffer.append("; TX=").append(OrbitSimulator.convert(tx))
					.append(' ');
			buffer.append("; TY=").append(OrbitSimulator.convert(ty))
					.append(' ');
			buffer.append("; TZ=").append(OrbitSimulator.convert(tz));
			return buffer.toString();
		}
	}

	/**
	 * Definiuje obiekt planety
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Planet extends Body {
		private static final long serialVersionUID = 6740789654470559672L;

		/**
		 * Odległość w peryhelionie od gwiazdy [m]
		 */
		public double perihelion = 0.0;

		/**
		 * Spłaszczenie orbity [0-1]
		 */
		public double eccentricity = 0.0;

		/**
		 * Czas okrążenia gwiazdy [s]
		 */
		public double period = 0.0;

		/**
		 * Obrót orbity [radiany]
		 */
		public double orbitRotation = 0.0;

		/**
		 * Przesunięcie fazy orbity [s]
		 */
		public double phaseShift = 0.0;

		/**
		 * Inklinacja orbity
		 */
		public double orbitInclination = 0.0;

		/**
		 * Precesja orbity
		 */
		public double orbitLongtitude = 0.0;

		/**
		 * Nachylenie orbity
		 */
		public double argumentOfPeriapsis = 0.0;

		/**
		 * Oblicza prawdziwą anomalię dla planety
		 * 
		 * @param meanAnomaly
		 *            średnia anomalia
		 * @param eccentricity
		 *            spłaszczenie orbity
		 * @return prawdziwa anomalia
		 * @author Radosław Ziembiński
		 */
		public double getTrueAnomaly(final double meanAnomaly,
				final double eccentricity) {
			// calculate Kepler equation
			final double precision = 0.001;
			double E = meanAnomaly;
			for (double difference = 1.0; Math.abs(difference) > precision;) {
				difference = E - eccentricity * Math.sin(E) - meanAnomaly;
				E -= difference / (1 - eccentricity * Math.cos(E));
			}
			return E;
		}

		/**
		 * Wyznacza położenie planet
		 * 
		 * @param star
		 *            gwiazda
		 * @param t
		 *            czas absolutny
		 * @author Radosław Ziembiński
		 */
		public void updatePlanetPosition(final Body star, final double t) {
			final double perihelionTime = (t + period * phaseShift) % period;
			final double timeFraction = perihelionTime / period;
			// System.out.println("Time fraction " + timeFraction);
			final double meanAnomaly = 2.0 * Math.PI * timeFraction;

			final double trueAnomaly = getTrueAnomaly(meanAnomaly, eccentricity);

			final double angle = 2.0 * Math.atan(Math.sqrt((1.0 + eccentricity)
					/ (1.0 - eccentricity))
					* Math.tan(trueAnomaly / 2.0));
			final double distanceFromStar = perihelion
					/ (1 + eccentricity * Math.cos(angle));

			// orbita w płaszczynie XY osadzona w koordynatach 0,0
			final double cx = distanceFromStar * Math.cos(angle);
			final double cy = distanceFromStar * Math.sin(angle);
			final double cz = 0.0;

			// macierz przekształcająca orbitę
			final double orbitLongtitudeSin = Math.sin(orbitLongtitude);
			final double orbitLongtitudeCos = Math.cos(orbitLongtitude);
			final double argumentOfPeriapsisSin = Math.sin(argumentOfPeriapsis);
			final double argumentOfPeriapsisCos = Math.cos(argumentOfPeriapsis);
			final double orbitInclinationSin = Math.sin(orbitInclination);
			final double orbitInclinationCos = Math.cos(orbitInclination);

			final double mx1 = orbitLongtitudeCos * argumentOfPeriapsisCos
					- orbitLongtitudeSin * orbitInclinationCos
					* argumentOfPeriapsisSin;
			final double mx2 = orbitLongtitudeSin * argumentOfPeriapsisCos
					+ orbitLongtitudeCos * orbitInclinationCos
					* argumentOfPeriapsisSin;
			final double mx3 = orbitInclinationSin * argumentOfPeriapsisSin;
			final double my1 = -orbitLongtitudeCos * argumentOfPeriapsisSin
					- orbitLongtitudeSin * orbitInclinationCos
					* argumentOfPeriapsisCos;
			final double my2 = -orbitLongtitudeSin * argumentOfPeriapsisSin
					+ orbitLongtitudeCos * orbitInclinationCos
					* argumentOfPeriapsisCos;
			final double my3 = orbitInclinationSin * argumentOfPeriapsisCos;
			final double mz1 = orbitInclinationSin * orbitLongtitudeSin;
			final double mz2 = -orbitInclinationSin * orbitLongtitudeCos;
			final double mz3 = orbitInclinationCos;

			// pozycja planety
			x = mx1 * cx + mx2 * cy + mx3 * cz + star.x;
			y = my1 * cx + my2 * cy + my3 * cz + star.y;
			z = mz1 * cx + mz2 * cy + mz3 * cz + star.z;

			// współrzędne ekranu
			y = -y;
		}
	}

	/**
	 * Populacja trajektorii posortowana względem wartości oceny
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Population extends TreeSet<Simulation> {

		/**
		 * Semafor służący do odgraniczania dostępu do obiektu populacji
		 */
		public Semaphore access = new Semaphore(1);

		/**
		 * Comment for <code>serialVersionUID</code>
		 */
		private static final long serialVersionUID = 2685656014512418951L;

		/**
		 * Konstruktor
		 * 
		 * @author Radosław Ziembiński
		 */
		public Population() {
			super(new Comparator<Simulation>() {
				@Override
				public int compare(final Simulation o1, final Simulation o2) {
					if (o1.probe.trajectoryEvaluation < o2.probe.trajectoryEvaluation) {
						return -1;
					} else if (o1.probe.trajectoryEvaluation > o2.probe.trajectoryEvaluation) {
						return 1;
					} else {
						return 0;
					}
				}
			});
		}
	}

	/**
	 * Definiuje obiekt sondy
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Probe extends Body {
		private static final long serialVersionUID = 1818876186501756185L;

		/**
		 * Prędkość sondy oś X [m/s]
		 */
		public double vx = 0.0;

		/**
		 * Prędkość sondy oś Y [m/s]
		 */
		public double vy = 0.0;

		/**
		 * Prędkość sondy oś Z [m/s]
		 */
		public double vz = 0.0;

		/**
		 * Maksymalny ciąg silnika sondy w [N]
		 */
		public double maxThrust = 0.0;

		/**
		 * Ilość paliwa sondy
		 */
		public double fuel = 0.0;

		/**
		 * Wydajność paliwa sondy
		 */
		public double fuelEfficiency = 0.0;

		/**
		 * Lista instrukcji sterujących sondy
		 */
		public SteeringInstructions steering = null;

		/**
		 * Bieżąca instrukcja
		 */
		public Iterator<Instruction> currentInctruction = null;

		/**
		 * Iterator po liście instrukcji
		 */
		public Instruction instruction = null;

		/**
		 * Flaga oznaczająca zderzenie z gwiazdą lub planetą
		 */
		public boolean crashed = false;

		/**
		 * Liczniki wskazujące na odwiedzenie okręślonej planety i gwiazdy
		 */
		public int visitedScore[] = null;

		/**
		 * Wartość oceny trajektorii sondy
		 */
		public double trajectoryEvaluation = 0.0;

		/**
		 * Konstruktor
		 * 
		 * @param steering
		 *            instrukcje sterujące
		 * @param planets
		 *            liczba planet
		 * @author Radosław Ziembiński
		 */
		public Probe(final SteeringInstructions steering, final int planets) {
			this.steering = steering;
			currentInctruction = steering.iterator();
			if (currentInctruction.hasNext()) {
				instruction = currentInctruction.next();
			}
			visitedScore = new int[planets + 1];
		}

		/**
		 * Oblicza siłę grawitacji dla ciała niebieskiego
		 * 
		 * @param index
		 *            indeks ciała niebieskiego
		 * @param b
		 *            obiekt definiujący ciało niebieskie
		 * @param distanceThreshold
		 *            próg odległości od którego liczy się odwiedziny dla
		 *            planety oznaczonej przez indeks
		 * @return odległość od ciała niebieskiego
		 * @author Radosław Ziembiński
		 */
		public double accumulateGravityForce(final int index, final Body b,
				final double distanceThreshold) {
			final double dx = b.x - x;
			final double dy = b.y - y;
			final double dz = b.z - z;

			final double d2 = dx * dx + dy * dy + dz * dz;
			final double d = Math.sqrt(d2);

			setStatus(b, d, distanceThreshold, index);

			final double force = OrbitSimulator.G * b.mass * mass / d2;
			final double potential = force / d;

			fx += potential * dx;
			fy += potential * dy;
			fz += potential * dz;

			return d;
		}

		/**
		 * Ustawia zmienne dotyczące zderzenia i odwiedzin planety oznaczone
		 * index
		 * 
		 * @param index
		 *            indeks ciała niebieskiego
		 * @param b
		 *            obiekt definiujący ciało niebieskie
		 * @param distanceThreshold
		 *            próg odległości od którego liczy się odwiedziny dla
		 *            planety oznaczonej przez indeks
		 * @return odległość od ciała niebieskiego
		 * @author Radosław Ziembiński
		 */
		public void setStatus(final Body b, final double d,
				final double distanceThreshold, final int index) {
			final double surfaceDistance = d - (radius + b.radius);
			if (surfaceDistance < distanceThreshold) {
				if (surfaceDistance <= 0) {
					crashed |= true;
				} else {
					visitedScore[index] += 1;
				}
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			final StringBuffer buffer = new StringBuffer();
			buffer.append("Fuel=").append(OrbitSimulator.convert(fuel))
					.append(' ');
			buffer.append("; EV=")
					.append(OrbitSimulator.convert(trajectoryEvaluation / 1E15))
					.append(' ');
			buffer.append("; X=").append(OrbitSimulator.convert(x)).append(' ');
			buffer.append("; Y=").append(OrbitSimulator.convert(y)).append(' ');
			buffer.append("; Z=").append(OrbitSimulator.convert(z)).append(' ');
			buffer.append("; VX=").append(OrbitSimulator.convert(vx))
					.append(' ');
			buffer.append("; VY=").append(OrbitSimulator.convert(vy))
					.append(' ');
			buffer.append("; VZ=").append(OrbitSimulator.convert(vz));
			return buffer.toString();
		}

		/**
		 * Aktualizuje położenie sondy
		 * 
		 * @param tick
		 *            czas w krokach działania algorytmu
		 * @param planets
		 *            zbiór planet
		 * @param star
		 *            gwiazda
		 * @param absoluteTime
		 *            czas absolutny w sekundach
		 * @param deltaTime
		 *            krok czasowy w sekundach
		 * @author Radosław Ziembiński
		 */
		public void updateProbePosition(final int tick, final Planet[] planets,
				final Star star, final double absoluteTime,
				final double deltaTime) {
			zeroeGravityForce();
			double distance = accumulateGravityForce(0, star, 0.0);

			if (planets != null) {
				int i = 1;
				for (final Planet planet : planets) {
					distance = Math
							.min(accumulateGravityForce(i, planet,
									OrbitSimulator.EVAUATION_DISTANCE_THRESHOLD),
									distance);
					i++;
				}
			}

			trajectoryEvaluation += (distance < OrbitSimulator.EVAUATION_DISTANCE_THRESHOLD) ? (distance)
					: (0.0);

			// uwzględnienie instrukcji sterujących i ciągu
			if ((steering != null) && (instruction != null)) {
				if (instruction.tick == tick) {
					if (fuel > 0) {
						double tfx = instruction.tx * maxThrust;
						double tfy = instruction.ty * maxThrust;
						double tfz = instruction.tz * maxThrust;
						final double fuelUsed = (Math.abs(instruction.tx)
								+ Math.abs(instruction.ty) + Math
									.abs(instruction.tz))
								* deltaTime
								/ (3.0 * fuelEfficiency);

						if (fuel > fuelUsed) {
							fuel -= fuelUsed;
						} else {
							final double remainingForce = fuel / fuelUsed;
							tfx = tfx * remainingForce;
							tfy = tfy * remainingForce;
							tfz = tfz * remainingForce;
							fuel = 0;
						}

						fx += tfx;
						fy += tfy;
						fz += tfz;
					}
					if (currentInctruction.hasNext()) {
						instruction = currentInctruction.next();
						// System.out.println("Probe parameters: " + this);
						// System.out.println("Probe instruction: " +
						// instruction);
					} else {
						instruction = null;
					}
				}
			}

			final double moment = deltaTime / mass;

			// zmiana prędkości i położenia sondy
			vx = vx + fx * moment;
			vy = vy + fy * moment;
			vz = vz + fz * moment;

			x = x + vx * deltaTime;
			y = y + vy * deltaTime;
			z = z + vz * deltaTime;
		}

		/**
		 * Czyszczenie zmiennych przechowujących informacje o oddziaływaniu
		 * 
		 * @author Radosław Ziembiński
		 */
		public void zeroeGravityForce() {
			fx = 0.0;
			fy = 0.0;
			fz = 0.0;
		}
	}

	/**
	 * Instancja symulacji
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Simulation implements Serializable {

		private static final long serialVersionUID = 2991912245463178583L;

		/**
		 * Definicja gwiazdy
		 */
		public Star star;

		/**
		 * Lista planet
		 */
		public Planet[] planets;

		/**
		 * Sonda
		 */
		public Probe probe;

		/**
		 * Granica okna wyświatlania
		 */
		public double minX = 0.0;

		/**
		 * Granica okna wyświatlania
		 */
		public double minY = 0.0;

		/**
		 * Granica okna wyświatlania
		 */
		public double minZ = 0.0;

		/**
		 * Granica okna wyświatlania
		 */
		public double maxX = 0.0;

		/**
		 * Granica okna wyświatlania
		 */
		public double maxY = 0.0;

		/**
		 * Granica okna wyświatlania
		 */
		public double maxZ = 0.0;

		/**
		 * Czas absolutny symulacji w sekundach
		 */
		public double absoluteTime = 0.0;

		/**
		 * Czas absolutny symulacjiw krokach
		 */
		public int tick = 0;

		/**
		 * Czas kroku symulacji w sekundach
		 */
		public double deltaTime = 1800.0;

		/**
		 * Oblicza położenia planet i sondy
		 * 
		 * @param tick
		 *            czas absolutny w krokach symulacji
		 * @param absoluteTime
		 *            czas absolutny w sekundach
		 * @param deltaTime
		 *            krok symulacji w sekundach
		 * @author Radosław Ziembiński
		 */
		public void calculate(final int tick, final double absoluteTime,
				final double deltaTime) {
			if (planets != null) {
				for (final Planet planet : planets) {
					planet.updatePlanetPosition(star, absoluteTime);
				}
			}
			if (probe != null) {
				probe.updateProbePosition(tick, planets, star, absoluteTime,
						deltaTime);
			}
		}

		/**
		 * Aktualizacja współrzędnych okna wyświetlania
		 * 
		 * @author Radosław Ziembiński
		 */
		public void updateBounds() {
			final double rd = 450000000000.0 * 1.5;
			minX = star.x - rd;
			minY = star.y - rd;
			minZ = star.z - rd;
			maxX = star.x + rd;
			maxY = star.y + rd;
			maxZ = star.z + rd;

			if (probe != null) {
				minX = Math.min(probe.x, minX);
				minY = Math.min(probe.y, minY);
				minZ = Math.min(probe.z, minZ);
				maxX = Math.max(probe.x, maxX);
				maxY = Math.max(probe.y, maxY);
				maxZ = Math.max(probe.z, maxZ);
			}

			if (planets != null) {
				for (final Planet planet : planets) {
					minX = Math.min(planet.x, minX);
					minY = Math.min(planet.y, minY);
					minZ = Math.min(planet.z, minZ);
					maxX = Math.max(planet.x, maxX);
					maxY = Math.max(planet.y, maxY);
					maxZ = Math.max(planet.z, maxZ);
				}
			}
		}

	}

	/**
	 * Definicja gwiazdy
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class Star extends Body {
		private static final long serialVersionUID = 6922141730547068102L;
	}

	/**
	 * Lista instrukcji sterujących posortowana względem wartości kroku
	 * symulacji
	 * 
	 * @author Radosław Ziembiński, 2012
	 */
	protected final static class SteeringInstructions extends
			TreeSet<Instruction> {

		private static final long serialVersionUID = 4983651154323551335L;

		/**
		 * Konstruktor
		 * 
		 * @author Radosław Ziembiński
		 */
		public SteeringInstructions() {
			super(new Comparator<Instruction>() {

				/*
				 * (non-Javadoc)
				 * 
				 * @see java.util.Comparator#compare(java.lang.Object,
				 * java.lang.Object)
				 */
				@Override
				public int compare(final Instruction o1, final Instruction o2) {
					if (o1.tick == o2.tick) {
						return 0;
					} else {
						if (o1.tick > o2.tick) {
							return 1;
						} else {
							return -1;
						}
					}
				}
			});
		}

		/**
		 * Tworzy kopię listy
		 * 
		 * @return kopia listy
		 * @throws Exception
		 * @author Radosław Ziembiński
		 */
		public SteeringInstructions cloneSteering() throws Exception {
			final SteeringInstructions clone = new SteeringInstructions();
			final Instruction[] instructions = this
					.toArray(new Instruction[] {});
			for (final Instruction instruction : instructions) {
				clone.add(instruction.cloneInstruction());
			}
			return clone;
		}

		/**
		 * Wstawia losową instrukcję do listy
		 * 
		 * @author Radosław Ziembiński
		 */
		public void insertRandom() throws Exception {
			final Instruction instruction = new Instruction();
			instruction.random();
			this.add(instruction);
		}

		/**
		 * Zmienia losową instrukcję na liście
		 * 
		 * @author Radosław Ziembiński
		 */
		public void modifyRandom() throws Exception {
			final int pos = OrbitSimulator.random.nextInt(this.size());
			final Instruction instruction = (this.toArray(new Instruction[] {}))[pos];
			this.remove(instruction);
			instruction.modifyRandom();
			this.add(instruction);
		}

		/**
		 * Zmienia listę instrukcji
		 * 
		 * @return zmieniona kopia listy instrukcji
		 * @author Radosław Ziembiński
		 */
		public SteeringInstructions mutate() throws Exception {
			final SteeringInstructions clone = this.cloneSteering();
			if (OrbitSimulator.random.nextDouble() < 0.5) {
				if (OrbitSimulator.random.nextDouble() < 0.5) {
					clone.removeRandom();
				} else {
					clone.insertRandom();
				}
			} else {
				clone.modifyRandom();
			}
			return clone;
		}

		/**
		 * Tworzy losową listę instrukcji
		 * 
		 * @param size
		 *            rozmiar listy
		 * @author Radosław Ziembiński
		 */
		public void randomList(final int size) {
			int tick = 1;
			for (int i = 0; i < size; i++) {
				final Instruction thrust = new Instruction();
				thrust.random();
				thrust.tick = tick;
				tick = tick + OrbitSimulator.random.nextInt(100) + 1;
				add(thrust);
			}
		}

		/**
		 * Usuwa instrukcję z listy w sposób losowy
		 * 
		 * @author Radosław Ziembiński
		 */
		public void removeRandom() throws Exception {
			final int pos = OrbitSimulator.random.nextInt(this.size());
			final Instruction instruction = (this.toArray(new Instruction[] {}))[pos];
			this.remove(instruction);
		}

	}

	/**
	 * Odleglość od ciała niebieskiego poniżej której sonda otrzymuje punktację
	 */
	protected final static double EVAUATION_DISTANCE_THRESHOLD = 10000000000.0;

	/**
	 * Stała grawitacji
	 */
	protected final static double G = 6.674E-11;

	private final static long serialVersionUID = -419155043919002453L;

	/**
	 * Maksymalna liczba kroków w symulacji
	 */
	protected final static int TICKS_MAX = 100000;

	/**
	 * Najlepsze rozwiązanie z poprzedniej symulacji
	 */
	protected Simulation currentBest = null;

	SimulationWorker simulationWorker = null;

	/**
	 * Semafor ograniczający liczbę równoległych procesów (aby wykorzystać
	 * wszystkie rdzenie procesora)
	 */
	protected final Semaphore semaphore = new Semaphore(4);

	/**
	 * Application wide random generator
	 */
	public static final Random random = new Random(System.nanoTime());

	/**
	 * Liczba populacji
	 */
	protected final static int POPULATIONS = 10;

	/**
	 * Rozmiar populacji
	 */
	protected final static int POPULATION_SIZE = 50;

	/**
	 * Rozmiar populacji najlepszych rozwiązań
	 */
	protected final static int BEST_POPULATION_SIZE = 10;

	/**
	 * Rozmiar losowej listy instrukcji
	 */
	protected final static int RANDOM_INSTRUCTION_LIST_SIZE = 1000;

	/**
	 * Prawdopodobieństwo utworzenia losowego rozwiązania
	 */
	protected final static double RANDOM_SOLUTION_PROBABILITY = 0.001;

	/**
	 * Przekształca liczbę zmiennoprzecinkową na łańcuch znaków
	 * 
	 * @param value
	 *            liczba
	 * @return łańcuch znaków
	 * @author Radosław Ziembiński
	 */
	protected static String convert(final double value) {
		return String.format("%.6G", value);
	}

	/**
	 * Przekształca tablicę na łańcuch znaków
	 * 
	 * @param table
	 *            tablica
	 * @return łańcuch znaków
	 * @author Radosław Ziembiński
	 */
	protected static String convert(final int[] table) {
		final StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < table.length; i++) {
			buffer.append(table[i]);
			if (i < table.length - 1) {
				buffer.append(';');
			}
		}
		return buffer.toString();
	}

	/**
	 * Tworzy nowy obiekt symulacji
	 * 
	 * @param instructions
	 *            lista instrukcji sterujących sondą
	 * @return obiekt symulacji
	 * @author Radosław Ziembiński
	 */
	protected final Simulation getSimulation(
			final SteeringInstructions instructions,
			final SimulationRequest parameters) {
		final Simulation simulation = new Simulation();
		final Star star = new Star(); // Sun
		star.x = 0.0;
		star.y = 0.0;
		star.x = 0.0;
		star.radius = 1392000000.0;
		star.mass = 2E30;

		final Planet p1 = new Planet(); // Mercury
		p1.perihelion = 46001200000.0;
		p1.eccentricity = 0.205630;
		p1.period = 87.9691 * 24.0 * 3600.0;
		p1.mass = 3.3022E23;
		p1.radius = 2439700.0;
		p1.orbitInclination = 6.34 * 2.0 * Math.PI / 360.0;
		p1.orbitLongtitude = 48.331 * 2.0 * Math.PI / 360.0;
		p1.argumentOfPeriapsis = 29.124 * 2.0 * Math.PI / 360.0;
		p1.updatePlanetPosition(star, 0);

		final Planet p2 = new Planet(); // Venus
		p2.perihelion = 107476259000.0;
		p2.eccentricity = 0.0068;
		p2.period = 224.700 * 24.00 * 3600.0;
		p2.mass = 4.8685E24;
		p2.radius = 6051800.0;
		p2.orbitInclination = 2.19 * 2.0 * Math.PI / 360.0;
		p2.orbitLongtitude = 76.670 * 2.0 * Math.PI / 360.0;
		p2.argumentOfPeriapsis = 54.852 * 2.0 * Math.PI / 360.0;
		p2.updatePlanetPosition(star, 0);

		final Planet p3 = new Planet(); // Earth
		p3.perihelion = 147098290000.0;
		p3.eccentricity = 0.01671123;
		p3.period = 365.256363004 * 24.00 * 3600.0;
		p3.mass = 5.9736E24;
		p3.radius = 6378000.0;
		p3.orbitInclination = 1.57869 * 2.0 * Math.PI / 360.0;
		p3.orbitLongtitude = 348.73936 * 2.0 * Math.PI / 360.0;
		p3.argumentOfPeriapsis = 114.20783 * 2.0 * Math.PI / 360.0;
		p3.updatePlanetPosition(star, 0);

		final Planet p4 = new Planet(); // Mars
		p4.perihelion = 206669000000.0;
		p4.eccentricity = 0.093315;
		p4.period = 686.971 * 24.00 * 3600.0;
		p4.mass = 6.4185E23;
		p4.radius = 3396200.0;
		p4.orbitInclination = 1.67 * 2.0 * Math.PI / 360.0;
		p4.orbitLongtitude = 49.562 * 2.0 * Math.PI / 360.0;
		p4.argumentOfPeriapsis = 286.537 * 2.0 * Math.PI / 360.0;
		p4.updatePlanetPosition(star, 0);

		simulation.star = star;
		simulation.planets = new Planet[] { p1, p2, p3, p4 };

		final Probe pr = new Probe(instructions, 4);
		pr.x = simulation.planets[parameters.getStartPlanetNumber()].x
				+ simulation.planets[parameters.getStartPlanetNumber()].radius
				+ 1000;
		pr.y = simulation.planets[parameters.getStartPlanetNumber()].y;
		pr.z = simulation.planets[parameters.getStartPlanetNumber()].z;
		pr.vx = parameters.getxVelocity();
		pr.vy = parameters.getyVelocity();
		pr.vz = parameters.getzVelocity();
		pr.mass = 100000.0;
		pr.radius = 20.0;
		pr.maxThrust = 30000.0;
		pr.fuel = 100.0;
		pr.fuelEfficiency = 3.0 * simulation.deltaTime;

		simulation.probe = pr;

		return simulation;
	}

	/**
	 * Przeprowadza optymalizację znajdując najlepszą trajektorię sondy przy
	 * pomocy prostego wielopopulacyjnego algorytmu genetycznego
	 * 
	 * @param args
	 *            argumenty programu
	 * @author Radosław Ziembiński
	 */
	public void startSimulation(final SimulationRequest simulationParams) {
		simulationWorker = new SimulationWorker(simulationParams);
		simulationWorker.start();
	}

	public SimulationAnimation stopAndGetBestResult() {
		simulationWorker.stopRunning();

		SimulationAnimation animation = new SimulationAnimation();
		animation.setSunPosition(new BodyPosition(currentBest.star.x,
				currentBest.star.y, currentBest.star.z));
		currentBest.tick = 0;
		currentBest.absoluteTime = 0;
		currentBest.calculate(currentBest.tick, currentBest.absoluteTime,
				currentBest.deltaTime);
		while (true) {
			if (currentBest.tick > OrbitSimulator.TICKS_MAX) {
				break;
			}

			currentBest.tick += 1;
			currentBest.absoluteTime += currentBest.deltaTime; // dodaj
																// godzinę
																// do czasu
																// absolutnego

			currentBest.calculate(currentBest.tick, currentBest.absoluteTime,
					currentBest.deltaTime);
			if (currentBest.probe.crashed) {
				break;
			}

			BodyPosition probePosition = new BodyPosition(currentBest.probe.x,
					currentBest.probe.y, currentBest.probe.z);
			probePosition.setAnimation(animation);
			animation.getProbePositions().add(probePosition);
			animation
					.getMercuryPositions()
					.add(new BodyPosition(currentBest.planets[0].x,
							currentBest.planets[0].y, currentBest.planets[0].z));
			animation
					.getVenusPositions()
					.add(new BodyPosition(currentBest.planets[1].x,
							currentBest.planets[1].y, currentBest.planets[1].z));
			animation
					.getEarthPositions()
					.add(new BodyPosition(currentBest.planets[2].x,
							currentBest.planets[2].y, currentBest.planets[2].z));
			animation
					.getMarsPositions()
					.add(new BodyPosition(currentBest.planets[3].x,
							currentBest.planets[3].y, currentBest.planets[3].z));

		}
		
		animation.setEvaluation(currentBest.probe.trajectoryEvaluation);

		return animation;
	}

	/**
	 * Przeprowadza symulację dokonując oceny trakktorii na podstawie listy
	 * instrukcji sterujących
	 * 
	 * @param instructions
	 *            lista instrukcji sterujacych
	 * @param view
	 *            czy animacja symulacji ma być pokazana w oknie
	 * @return obiekt symulacji
	 * @author Radosław Ziembiński
	 */
	protected Simulation simulate(final SteeringInstructions instructions,
			SimulationRequest parameters) {
		final Simulation simulation = getSimulation(instructions, parameters);

		while (true) {
			if (simulation.tick > OrbitSimulator.TICKS_MAX) {
				break;
			}

			simulation.tick += 1;
			simulation.absoluteTime += simulation.deltaTime; // dodaj
																// godzinę
																// do czasu
																// absolutnego

			simulation.calculate(simulation.tick, simulation.absoluteTime,
					simulation.deltaTime);
			if (simulation.probe.crashed) {
				break;
			}
		}

		if ((simulation.probe.trajectoryEvaluation == 0.0)
				|| (simulation.probe.crashed)) {
			// kara za brak przelotu obok jakiejkolwiek planety lub
			// zderzenie
			simulation.probe.trajectoryEvaluation = Double.MAX_VALUE;
		} else {
			// przekalowanie wyniku i obliczenie wyniku końcowego
			double sum = 0.0;
			for (final int value : simulation.probe.visitedScore) {
				sum += value;
			}

			double entropy = 0.0;
			for (final int value : simulation.probe.visitedScore) {
				if (value > 0) {
					entropy += (value / sum) * -Math.log(value / sum);
				}
			}

			final double cumulation = simulation.probe.trajectoryEvaluation;

			simulation.probe.steering.size();

			simulation.probe.trajectoryEvaluation = Math.sqrt(Math
					.sqrt(cumulation)) / (sum * entropy + 1.0);
		}
		return simulation;
	}

	private class SimulationWorker extends Thread {
		private boolean keepRuning;
		private SimulationRequest simulationParams;

		public SimulationWorker(SimulationRequest simulationParams) {
			keepRuning = true;
			this.simulationParams = simulationParams;
		}

		public void stopRunning() {
			keepRuning = false;
		}

		@Override
		public void run() {
			try {

				final Population bestPopulation = new Population();
				final Vector<Population> populations = new Vector<Population>();
				for (int i = 0; i < OrbitSimulator.POPULATIONS; i++) {
					populations.add(new Population());
				}

				int currentPopulation = 0;
				while (keepRuning) {
					final Population population = populations
							.elementAt(currentPopulation);

					SteeringInstructions instructions = null;
					if (OrbitSimulator.random.nextDouble() < OrbitSimulator.RANDOM_SOLUTION_PROBABILITY) {
						// utworzenie losowego rozwiązania
						instructions = new SteeringInstructions();
						instructions
								.randomList(OrbitSimulator.RANDOM_INSTRUCTION_LIST_SIZE);
					} else {
						// mutacja istniejącego rozwiązania
						final int i = (int) (OrbitSimulator.random.nextDouble() * population
								.size());
						if (i == 0) {
							continue;
						}

						population.access.acquire();
						final Simulation simulation = (population
								.toArray(new Simulation[] {}))[i];
						instructions = simulation.probe.steering.mutate();
						population.access.release();
					}

					if (instructions == null) {
						continue;
					}

					semaphore.acquire();
					final SteeringInstructions instructionsFinalized = instructions;
					final Thread thread = new Thread() {
						/*
						 * (non-Javadoc)
						 * 
						 * @see java.lang.Thread#run()
						 */
						@Override
						public void run() {

							try {
								final Population p = population;
								Thread.currentThread().setPriority(
										Thread.MIN_PRIORITY);

								// wykonanie symulacji
								final Simulation simulation = simulate(
										instructionsFinalized, simulationParams);

								p.access.acquire();
								bestPopulation.access.acquire();
								if (simulation.probe.trajectoryEvaluation < Double.MAX_VALUE) {
									p.add(simulation);
									bestPopulation.add(simulation);
								}

								// ograniczenie rozmiaru populacji
								while (p.size() > OrbitSimulator.POPULATION_SIZE) {
									p.remove(p.last());
								}

								while (bestPopulation.size() > OrbitSimulator.BEST_POPULATION_SIZE) {
									bestPopulation
											.remove(bestPopulation.last());
								}

								Simulation best = null;
								if (bestPopulation.size() > 0) {
									best = bestPopulation.first();
								}
								p.access.release();
								bestPopulation.access.release();

								if (currentBest != best) {
									currentBest = best;
								}

							} catch (final Exception e) {
								System.out.println(e.getLocalizedMessage());
								e.printStackTrace();
							}
							semaphore.release();
						}
					};
					thread.start();

					currentPopulation++;
					if (currentPopulation >= populations.size()) {
						currentPopulation = 0;
					}
				}
			} catch (final Exception e) {
				System.out.println(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
	}
}

package put.tpsi.orbitsimulator.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import put.tpsi.orbitsimulator.ejb.UserRegistryEJB;
import put.tpsi.orbitsimulator.entities.User;

@ManagedBean
@RequestScoped
public class RegisterController {

	@EJB
	private UserRegistryEJB userRegistryEJB;
	
	private User user = new User();

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String register() {
		user.setLoggedFlag(0);
		if (!userRegistryEJB.checkIfExist(user)) {
			userRegistryEJB.addNewUser(user);
			return "user-added.xhtml?faces-redirect=true";
		}
		else {
			return "user-exist.xhtml?faces-redirect=true";
		}
	}
	
}

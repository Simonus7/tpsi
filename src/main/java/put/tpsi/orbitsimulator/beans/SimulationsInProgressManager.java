package put.tpsi.orbitsimulator.beans;

import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import put.tpsi.orbitsimulator.logic.OrbitSimulator;

@ManagedBean(name = "simulationsInProgressManager", eager = true)
@ApplicationScoped
public class SimulationsInProgressManager {
	private Map<String, OrbitSimulator> simulationsInProgress = new HashMap<String, OrbitSimulator>();

	public boolean registerNewSimulation(String userName) {
		boolean result = false;

		if (!simulationsInProgress.containsKey(userName)) {
			simulationsInProgress.put(userName, new OrbitSimulator());
			result = true;
		}

		return result;
	}

	public OrbitSimulator getSimulatorForUser(String userName) {
		return simulationsInProgress.get(userName);
	}

	public void clearSimulation(String userName) {
		simulationsInProgress.remove(userName);
	}
}

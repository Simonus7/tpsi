package put.tpsi.orbitsimulator.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import put.tpsi.orbitsimulator.ejb.UserRegistryEJB;
import put.tpsi.orbitsimulator.entities.User;

@ManagedBean
@SessionScoped
public class LoginController {

	@EJB
	private UserRegistryEJB userRegistryEJB;

	@ManagedProperty(value = "#{ses}")
	private SessionBean session;

	private User user = new User();

	public User getUser() {
		return user;
	}

	public SessionBean getSession() {
		return session;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public String login() {
		user.setLoggedFlag(0);
		User userx = userRegistryEJB.getUser(user);
		if (userx == null)
			return "login-error.xhtml?faces-redirect=true";
		else {
			if (!user.getPass().equals(userx.getPass()))
				return "login-error-pass.xhtml?faces-redirect=true";
			else {
				if (userx.getLoggedFlag() == 1) {
					// FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
					// userRegistryEJB.setLoggedOut(userx);
					return "login-error-logged.xhtml?faces-redirect=true";
				} else {
					session.setUser(userx);
					session.setUserLogged(true);
					userRegistryEJB.setLoggedIn(userx);
					return "simulation.xhtml?faces-redirect=true";
				}
			}
		}
	}
}

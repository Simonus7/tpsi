package put.tpsi.orbitsimulator.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import put.tpsi.orbitsimulator.ejb.UserRegistryEJB;
import put.tpsi.orbitsimulator.entities.User;

@SuppressWarnings("serial")
@ManagedBean(name="ses")
@SessionScoped
public class SessionBean implements Serializable {

	@EJB
	private UserRegistryEJB userRegistryEJB;
	
	private User user;
	private Boolean userLogged = false;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getUserLogged() {
		return userLogged;
	}

	public void setUserLogged(Boolean userLogged) {
		this.userLogged = userLogged;
	}
	
	public void checkUser() throws IOException {
		if (!userLogged) {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		}
	}
	
	public String logout() {
		userRegistryEJB.setLoggedOut(user);
		userLogged = false;
		user = null;
		return "login.xhtml?faces-redirect=true";
	}

}

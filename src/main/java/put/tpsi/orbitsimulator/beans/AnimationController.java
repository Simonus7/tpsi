package put.tpsi.orbitsimulator.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import put.tpsi.orbitsimulator.ejb.SimulationAnimationRegistryEJB;
import put.tpsi.orbitsimulator.entities.BodyPosition;
import put.tpsi.orbitsimulator.entities.SimulationAnimation;

@ManagedBean
@SessionScoped
public class AnimationController {

	@EJB
	private SimulationAnimationRegistryEJB ejb;
	
	@ManagedProperty(value="#{ses}")
	private SessionBean session;
	
	private SimulationAnimation simulationAnimation;
	
	private List<SimulationAnimation> simulations = new ArrayList<SimulationAnimation>();

	public SimulationAnimation getSimulationAnimation() {
		return simulationAnimation;
	}

	public void setSimulationAnimation(SimulationAnimation simulationAnimation) {
		this.simulationAnimation = simulationAnimation;
	}
	
	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}
	
	public List<SimulationAnimation> getSimulations() {
		simulations = ejb.getUserSimulations(session.getUser().getId());
		return simulations;
	}
}

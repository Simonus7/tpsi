package put.tpsi.orbitsimulator.beans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import put.tpsi.orbitsimulator.ejb.SimulationAnimationRegistryEJB;
import put.tpsi.orbitsimulator.entities.SimulationAnimation;
import put.tpsi.orbitsimulator.entities.SimulationRequest;
import put.tpsi.orbitsimulator.logic.OrbitSimulator;

@ManagedBean
@ViewScoped
public class SimulationManager {
	private static final String NO_SIMULATION_STATUS = "Nie masz obecnie żadnej trwającej symulacji";
	private static final String SIMULATION_IN_PROGRESS_STATUS = "Trwa symulacja...";
	private static final String OBTAINING_STATUS = "Pobietam status...";

	@EJB
	private SimulationAnimationRegistryEJB animationRegistry;

	@ManagedProperty(value = "#{ses}")
	private SessionBean session;

	private SimulationRequest simulationRequest = new SimulationRequest();

	private String simulationStatus = OBTAINING_STATUS;

	public SimulationRequest getSimulationRequest() {
		return simulationRequest;
	}

	public void setSimulationRequest(SimulationRequest simulationRequest) {
		this.simulationRequest = simulationRequest;
	}

	public String getSimulationStatus() {
		return simulationStatus;
	}

	public void setSimulationStatus(String simulationStatus) {
		this.simulationStatus = simulationStatus;
	}

	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public void startNewSimulation() {
		SimulationsInProgressManager simulations = (SimulationsInProgressManager) FacesContext
				.getCurrentInstance().getExternalContext().getApplicationMap()
				.get("simulationsInProgressManager");
		boolean created = simulations.registerNewSimulation(session.getUser()
				.getLogin());
		if (created) {
			OrbitSimulator simulator = simulations.getSimulatorForUser(session
					.getUser().getLogin());
			simulator.startSimulation(simulationRequest);
		}
	}

	public void stopCurrentSimulation() {
		SimulationsInProgressManager simulations = (SimulationsInProgressManager) FacesContext
				.getCurrentInstance().getExternalContext().getApplicationMap()
				.get("simulationsInProgressManager");

		OrbitSimulator simulator = simulations.getSimulatorForUser(session
				.getUser().getLogin());
		if (simulator != null) {
			System.out.println("Przetwarzam wynik...");
			SimulationAnimation animation = simulator.stopAndGetBestResult();
			storeAnimationInDB(animation);
			System.out.println("Gotowe");
			simulations.clearSimulation(session.getUser().getLogin());
		}
	}

	private void storeAnimationInDB(SimulationAnimation animation) {
		animation.setUser(session.getUser());
		animationRegistry.addNewAnimation(animation);
	}

	public void updateSimulationStatus() {
		SimulationsInProgressManager simulations = (SimulationsInProgressManager) FacesContext
				.getCurrentInstance().getExternalContext().getApplicationMap()
				.get("simulationsInProgressManager");
		if (simulations.getSimulatorForUser(session.getUser().getLogin()) == null) {
			simulationStatus = NO_SIMULATION_STATUS;
		} else {
			simulationStatus = SIMULATION_IN_PROGRESS_STATUS;
		}
	}
}

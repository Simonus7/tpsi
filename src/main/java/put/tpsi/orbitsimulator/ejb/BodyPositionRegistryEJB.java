package put.tpsi.orbitsimulator.ejb;

import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import put.tpsi.orbitsimulator.entities.BodyPosition;

@Stateless
@RequestScoped
public class BodyPositionRegistryEJB {
	@PersistenceContext
	private EntityManager em;

	public void addNewBodyPosition(BodyPosition bodyPosition) {
		em.persist(bodyPosition);
	}

}

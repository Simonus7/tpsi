package put.tpsi.orbitsimulator.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import put.tpsi.orbitsimulator.entities.SimulationAnimation;

@Stateless
@RequestScoped
public class SimulationAnimationRegistryEJB {
	@PersistenceContext(name = "orbitEm")
	private EntityManager em;

	public void addNewAnimation(SimulationAnimation animation) {
		em.persist(animation);
	}
	
	@SuppressWarnings("unchecked")
	public List<SimulationAnimation> getUserSimulations(Long id) {
		Query q = em.createQuery("select s from animation s where s.user.id = :id");
		q.setParameter("id", id);
		List<SimulationAnimation> simulations = q.getResultList();
		//System.out.println(simulations.size());
		if (simulations == null) {
			return new ArrayList<SimulationAnimation>();
		}
		return simulations;
	}
}

package put.tpsi.orbitsimulator.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import put.tpsi.orbitsimulator.entities.User;

@Stateless
@RequestScoped
@SuppressWarnings("unchecked")
public class UserRegistryEJB {

	@PersistenceContext(name = "orbitEm")
	private EntityManager em;
	
	public void addNewUser(User user) {
		em.persist(user);
	}
	
	public User getUser(User user) {
		Query q = em.createQuery("select b from User b where b.login=:login");
		q.setParameter("login", user.getLogin());
		List<User> users = q.getResultList();
		if (users.size() > 0)
			return users.get(0);
		else
			return null;
	}
	
	public void setLoggedIn(User user) {
			User userx = em.find(User.class, user.getId());
			userx.setLoggedFlag(1);
			em.persist(userx);

	}
	
	public void setLoggedOut(User user) {
		User userx = em.find(User.class, user.getId());
		userx.setLoggedFlag(0);
		em.persist(userx);
	}
	
	public Boolean checkIfExist(User user) {
		Query q = em.createQuery("select b from User b where b.login=:login");
		q.setParameter("login", user.getLogin());
		List<User> users = q.getResultList();
		if (users.size() > 0)
			return true;
		else
			return false;
	}
}

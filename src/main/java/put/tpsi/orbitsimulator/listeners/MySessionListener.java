package put.tpsi.orbitsimulator.listeners;

import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import put.tpsi.orbitsimulator.beans.SessionBean;
import put.tpsi.orbitsimulator.ejb.UserRegistryEJB;

public class MySessionListener implements HttpSessionListener{
	
	@EJB
	private UserRegistryEJB userRegistryEJB;

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("Session created");
		prepareLogoutInfo(arg0.getSession());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		SessionBean session = (SessionBean) arg0.getSession().getAttribute("ses");
		userRegistryEJB.setLoggedOut(session.getUser());
		System.out.println("Session destroyed");
	}
	
	public void prepareLogoutInfo(HttpSession session) {
		
	}

}

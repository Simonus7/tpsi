package put.tpsi.orbitsimulator.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class BodyPosition {
	@Id
	@GeneratedValue
	private int id;
	private double x;
	private double y;
	private double z;

	@JoinColumn(name = "animation_id", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private SimulationAnimation animation;

	public BodyPosition() {

	}

	public BodyPosition(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public SimulationAnimation getAnimation() {
		return animation;
	}

	public void setAnimation(SimulationAnimation animation) {
		this.animation = animation;
	}
}

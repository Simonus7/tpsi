package put.tpsi.orbitsimulator.entities;


public class SimulationRequest {
	private int startPlanetNumber;
	private int xVelocity;
	private int yVelocity;
	private int zVelocity;

	public int getStartPlanetNumber() {
		return startPlanetNumber;
	}

	public int getxVelocity() {
		return xVelocity;
	}

	public int getyVelocity() {
		return yVelocity;
	}

	public int getzVelocity() {
		return zVelocity;
	}

	public void setStartPlanetNumber(int startPlanetNumber) {
		this.startPlanetNumber = startPlanetNumber;
	}

	public void setxVelocity(int xVelocity) {
		this.xVelocity = xVelocity;
	}

	public void setyVelocity(int yVelocity) {
		this.yVelocity = yVelocity;
	}

	public void setzVelocity(int zVelocuty) {
		this.zVelocity = zVelocuty;
	}
}

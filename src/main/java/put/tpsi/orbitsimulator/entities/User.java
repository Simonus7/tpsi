package put.tpsi.orbitsimulator.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "orbit_users")
public class User implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	private String login;
	private String pass;
	private int loggedFlag;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<SimulationAnimation> animations = new ArrayList<SimulationAnimation>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getLoggedFlag() {
		return loggedFlag;
	}

	public void setLoggedFlag(int loggedFlag) {
		this.loggedFlag = loggedFlag;
	}

	public List<SimulationAnimation> getAnimations() {
		return animations;
	}

	public void setAnimations(List<SimulationAnimation> animations) {
		this.animations = animations;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("User");
		sb.append(", login='").append(login);
		sb.append(", pass='").append(pass);
		sb.append(", flag='").append(loggedFlag);
		return sb.toString();
	}
}

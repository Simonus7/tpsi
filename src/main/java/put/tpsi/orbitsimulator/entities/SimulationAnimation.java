package put.tpsi.orbitsimulator.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity(name = "animation")
public class SimulationAnimation {
	@Id
	@GeneratedValue
	private int id;

	@JoinColumn(name = "orbit_user_id", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private User user;
	
	private double evaluation;

	@Transient
	private BodyPosition sunPosition;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "animation")
	private List<BodyPosition> probePositions = new ArrayList<BodyPosition>();

	@Transient
	private List<BodyPosition> mercuryPositions = new ArrayList<BodyPosition>();

	@Transient
	private List<BodyPosition> venusPositions = new ArrayList<BodyPosition>();

	@Transient
	private List<BodyPosition> earthPositions = new ArrayList<BodyPosition>();

	@Transient
	private List<BodyPosition> marsPositions = new ArrayList<BodyPosition>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(double evaluation) {
		this.evaluation = evaluation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BodyPosition getSunPosition() {
		return sunPosition;
	}

	public void setSunPosition(BodyPosition sunPosition) {
		this.sunPosition = sunPosition;
	}

	public List<BodyPosition> getProbePositions() {
		return probePositions;
	}

	public void setProbePositions(List<BodyPosition> probePositions) {
		this.probePositions = probePositions;
	}

	public List<BodyPosition> getMercuryPositions() {
		return mercuryPositions;
	}

	public void setMercuryPositions(List<BodyPosition> mercuryPositions) {
		this.mercuryPositions = mercuryPositions;
	}

	public List<BodyPosition> getVenusPositions() {
		return venusPositions;
	}

	public void setVenusPositions(List<BodyPosition> venusPositions) {
		this.venusPositions = venusPositions;
	}

	public List<BodyPosition> getEarthPositions() {
		return earthPositions;
	}

	public void setEarthPositions(List<BodyPosition> earthPositions) {
		this.earthPositions = earthPositions;
	}

	public List<BodyPosition> getMarsPositions() {
		return marsPositions;
	}

	public void setMarsPositions(List<BodyPosition> marsPositions) {
		this.marsPositions = marsPositions;
	}
}
